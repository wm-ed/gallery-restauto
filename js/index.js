const elGallery = $('.gallery');
const elGalleryBase = $('.js--gallery-base');
const elGalleryMiniature = $('.js--gallery-miniature');
const elGalleryButton = $('.js--toggle-miniature');

const elGalleryClone = elGallery.clone();

elGalleryBase.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    speed: 100,
    infinite: false,
    asNavFor: '.js--gallery-miniature',
    prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492.004 492.004"><path d="M281.2 226.804L62.252 7.86C57.188 2.792 50.428 0 43.22 0S29.252 2.792 24.188 7.86L8.064 23.98c-10.492 10.504-10.492 27.576 0 38.064L191.92 245.9 7.86 429.96C2.796 435.028 0 441.784 0 448.988c0 7.212 2.796 13.968 7.86 19.04l16.124 16.116c5.068 5.068 11.824 7.86 19.032 7.86s13.968-2.792 19.032-7.86L281.2 265c5.076-5.084 7.864-11.872 7.848-19.088.016-7.244-2.772-14.028-7.848-19.108z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492.004 492.004"><path d="M281.2 226.804L62.252 7.86C57.188 2.792 50.428 0 43.22 0S29.252 2.792 24.188 7.86L8.064 23.98c-10.492 10.504-10.492 27.576 0 38.064L191.92 245.9 7.86 429.96C2.796 435.028 0 441.784 0 448.988c0 7.212 2.796 13.968 7.86 19.04l16.124 16.116c5.068 5.068 11.824 7.86 19.032 7.86s13.968-2.792 19.032-7.86L281.2 265c5.076-5.084 7.864-11.872 7.848-19.088.016-7.244-2.772-14.028-7.848-19.108z"/></svg></button>',
});

elGalleryMiniature.slick({
    asNavFor: '.js--gallery-base',
    arrows: false,
    speed: 100,
    focusOnSelect: true,
    infinite: false,
});

const getWidthWindow = $(window).width();
const getCountMiniatures = elGalleryMiniature.find('.slick-slide').length;
const getLangShow = elGalleryButton.data('lang-show');
const getLangHide = elGalleryButton.data('lang-hide');
const variationsCountMiniatures = [
    { medias: [768, 992], count: 4 },
    { medias: [992, 1200], count: 5 },
    { medias: [1200, 1560], count: 2 },
    { medias: [1560, 3600], count: 7 },
];

variationsCountMiniatures.map(variation => {
    const isMedia = getWidthWindow >= variation.medias[0] && getWidthWindow < variation.medias[1];
    const isCount = getCountMiniatures > variation.count;
    if(isMedia && isCount) {
        elGalleryButton.addClass('active');
    }
})

$(document).on('click', '.js--toggle-miniature', function () {
    if (!elGalleryMiniature.hasClass('show')) {
        elGalleryMiniature.addClass('show');
        $(this).addClass('show');
        $(this).find('span').text(getLangHide);
    } else {
        elGalleryMiniature.removeClass('show');
        $(this).removeClass('show');
        $(this).find('span').text(getLangShow);
    }
})

// MODAL

const setWidthScrollbar = (audit = false) => {
    if (audit) {
        const beforeWidth = $('body').width();
        $('body').css({
            'overflow': 'hidden',
            'width': '100vw',
            'height': '100vh',
        });
        const afterWidth = $('body').width();
        const widthScrollbar = afterWidth - beforeWidth;
        $('body').addClass('auditActive').css({ 'padding-right': `${widthScrollbar}px` });
    } else {
        $('body').removeClass('auditActive').css({
            'overflow': 'visible',
            'width': '100%',
            'height': 'auto',
            'padding-right': '0px'
        });
    }
}

$(document).on('beforeShow.fb', () => { setWidthScrollbar(true); })
$(document).on('afterClose.fb', () => { setWidthScrollbar(); })

elGalleryClone.find('.js--gallery-base').toggleClass('js--gallery-base js--modal-base');
elGalleryClone.find('.js--gallery-miniature').toggleClass('js--gallery-miniature js--modal-miniature');
elGalleryClone.addClass('gallery-modal');

$(document).on('click', '.js--gallery-base .gallery-base__item', function () {
    const elBaseItem = $(this);
    $.fancybox.open(elGalleryClone, {
        touch: false,
        type: 'inline',
        animationDuration: 375,
        smallBtn: false,
        autoFocus: false,
        protect: true,
        afterLoad: function () {
            const getBaseItemId = elBaseItem.closest('[data-base]').data('base');
            const elBaseModal = $('.js--modal-base [data-base="' + getBaseItemId + '"]');
            const elContentBase = $('.js--modal-base').closest('.gallery-base');
            const posTop = elContentBase.scrollTop() + elBaseModal.position().top;
            elContentBase.animate({ scrollTop: posTop }, 0);
        }
    });
});

$(document).on('click', '.js--modal-miniature [data-miniature]', function () {
    const getMiniatureId = $(this).data('miniature');
    const elBase = $('.js--modal-base [data-base="' + getMiniatureId + '"]');
    const elContentBase = $('.js--modal-base').closest('.gallery-base');
    const posTop = elContentBase.scrollTop() + elBase.position().top;
    elContentBase.animate({ scrollTop: posTop }, 300);
})